# Campus Contest 3
## RIGAUD Jean-Baptiste
### Switch IT B1 au Campus d'Aix-En-Provence
##
20/05/2021 : Premier jour :
- 10h30 : Création du dépôt git et dépôt du dossier contenant les fichiers et dossiers pour le site en HTML CSS.
- 11h36 : Ajout des premières modification au niveau de l'header du portfolio.
- 11h54 : Idée venue : Création d'un double du HTML pour avoir une version française en plus de la version normale que je fais en anglais
- 12h30 : mise à jour des fichiers avant de prendre une pause déjeuner.
- 14h17 : ajout du fichier pour les images.
- 15h55 : Mise à jour du fichier principal + ajout du fichier concernant les contacts.
- 16h22 : Petite mise à jour sur les fichiers déjà présents + ajouts du fichier "About me" et fin de la journée
##
21/05/2021 Deuxième jour :
- 10h56 : Grosse mise à jour et Ajout des pages Skills et My World ainsi que des images
- 11h31 : Dernière grosse mise à jour : ajout des fichiers traduits en français et arrangement des fichiers HTML pour le conseil W3C (pas d'erreurs trouvées du début à la fin).
- 11h46 : fin du dépôt et des changements mineurs et relecture


### Pour plus de facilité, ouvrir les fichiers Contest.html ou ContestFR.html pour tout observer sans avoir plusieurs fenêtre pour le même site.

- 11h48 : Fin du Contest, envoie par mail dans les minutes qui suivent.
